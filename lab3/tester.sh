#!/bin/bash

for f in testovi/* 
do
  cp $f/test.in .
  cp $f/test.out .
  ./analizator < test.in > outf 2>/dev/null
  echo "testiram $f:"
  diff -qb outf test.out
  if [ $? -ne '0' ]
  then
    echo "greska!"
    exit 0
  fi

done
