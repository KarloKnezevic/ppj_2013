
#ifndef AUTOMATA_H
#define AUTOMATA_H

#include "utils.h"

#include <string>
#include <vector>
#include <set>
#include <utility>
#include <queue>
#include <map>


// definicija sucelja razreda Automata, vidi implementaciju
// za detalje

class Automata {

private:
   // regularni izraz koji automat prepoznaje
   std::string regex;
   // je i-ti znak terminalni
   std::vector<bool> term;

   // "komad" automata koji predstavlja cijeli automat
   // part je inace par stanja (pocetno, konacno)
   Part all;
   Part parse(int, int);

   int find_balanced(int, int, char);
   void pretprocess(std::string);

   std::set<State> curr;
   std::set<State> get_eps(State);

   void _output(std::map<State, int>&, State);
public:

   Automata(std::string);

   bool next(char);
   bool lost();
   void clear();

   void output();
};

#endif

