#include <cstdio>
#include <cstring>
#include <cstdlib>

#include <string>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

//#define LL_DEBUG
#include "log.h"

#define FORS(i, s, z) for (int i = s.find(z); i != string::npos; i = s.find(z, i + 1))

// Ovaj string je kod iz analizator_bp.cpp i ispisuje
// se nakon nizova koji ovise o jeziku koji analiziramo.
// ==================================
// ==================================

// jedan redak
string line;
// pocetno stanje
string source;
// lista svih redaka
vector< string > lines;
// lista svih stanja
vector< string > states;
// lista uniformnih znakova
vector< string > uniforms;

// opis pravila

struct rule {
  // stanje u kojem se primjenjuje pravilo
  string state;
  // regularni izraz za prepoznavanje pravila
  string regex;

  // ime (uniformnog znaka) ako postoji, inace "-"
  string name;
  // u koje stanje vodi ovo pravilo ($ ako ne vodi nikamo)
  string toState;

  // znaci li ovo novi redak?
  bool newLine;
  // ako se ne vracamo natrag onda -1, inace broj
  // znakova koji se trebaju smatrati procitanim nakon
  // primjene pravila
  int goBack;

  rule() { clear(); }

  void clear() {
    name = "-";
    toState = "$";
    newLine = false;
    goBack = -1;
  }
};

// lista pravila
vector< rule > rules;

// jedna definicija... ima ime i regularni izraz
struct definition {
  string name;
  string regex;

  definition(string n, string r) {
    name = n;
    regex = r;
  }
};

// lista svih definicija
vector< definition > definitions;

// funkcija za izrezivanje prefiksa od pat
// sve do prve pojave stringa s
string cut(string pat, string s) {
  int pos = s.find(pat) + pat.length();
  return s.substr(pos, s.length() - pos);
}

// funkcija za izrezivanje komada stringova po razmacima
// nista pametno, moze se na 1000 nacina
vector< string > multicut(string s) {
  vector< string > ret;
  FORS (i, s, " ") {
    int to = s.find(" ", i + 1);
    if (to == string::npos) to = s.size();
    ret.push_back(s.substr(i + 1, to - i - 1));
  }
  return ret;
}

// vraca regularni izraz koji odgovara definiciji s
string get(string s) {
  for (int i = 0; i < definitions.size(); ++i) 
    if (definitions[i].name == s) 
      return definitions[i].regex;
  return "$$ ERROR $$";
}

// pretvori regularnu definiciju u regularni izraz
// uzastopnim zamjenama... nista pametno, samo direktna implementacija

string resolve(string s) {
  vector< bool > e;
  e.push_back(0);
  // pronadji koji znakove koji su posebni, e[i] == 1 ako je i-ti znak poseban
  for (int i = 1; i < s.size(); ++i) {
    if (e[i - 1] || s[i - 1] != '\\') 
      e.push_back(0);
    else
      e.push_back(1);
  }
  // dijelovi stringa koje treba zamijeniti
  vector< pair< int, int > > subs;
  FORS (i, s, "{") {
    if (e[i]) continue;
    int next = s.find("}", i);
    // dodaj isjecak koji treba zamijeniti
    subs.push_back(make_pair(i, next - i + 1));
  }
  // idi kroz isjecke unatrag i zamijeni...
  for (int i = subs.size() - 1; i >= 0; --i) 
    s.replace(subs[i].first, subs[i].second, 
        "(" + get(s.substr(subs[i].first, subs[i].second)) + ")");
  return s;
}

// pozicija stringa state u nizu states
int getIdx(string state) {
  int ret = lower_bound(states.begin(), states.end(), state) - states.begin();
  if (ret == states.size() || state == "$")
    return -1;
  return ret;
}

// za posebne znakove kao npr. " koje treba pisati kao \" ili
string escaped(string s) {
  string ret = "";
  for (int i = 0; i < s.size(); ++i) {
    if (s[i] == '\\' || s[i] == '"')
      ret += '\\';
    ret += s[i];
  }
  return ret;
}

int main(void)
{
  // otvori fajl koji generiramo
  freopen("analizator/analizator.cpp", "w", stdout);

  // ucitaj cijeli ulaz liniju po liniju
  while (getline(cin, line)) 
    lines.push_back(line);

  rule r;

  /**
    Ovo je obicno parsiranje, dakle nista pametno.
    Jednostavno ucitavamo opis jezika u formatu opisanom u uputi.
    Svako pravilo koje prepoznamo ubacimo u listu pravila r.
    */

  int rulePos = -1;
  for (int i = 0; i < lines.size(); ++i) {
    if (rulePos == 2) {
      r.name = lines[i];
    }
    else if (lines[i] == "NOVI_REDAK") {
      r.newLine = true;
    }
    else if (lines[i].find("UDJI") == 0) {
      r.toState = cut(" ", lines[i]);
    }
    else if (lines[i].find("VRATI_SE") == 0) {
      r.goBack = atoi(cut(" ", lines[i]).c_str());
    }
    else if (lines[i] == "}") {
      rulePos = -1;
      rules.push_back(r);
      r.clear();
    }
    else if (lines[i] == "{") {
    }
    else if (lines[i][0] == '<') {
      rulePos = 0;
      r.state = lines[i].substr(1, lines[i].find(">") - 1);
      r.regex = resolve(cut(">", lines[i]));
    }
    else if (lines[i].find("\%X") == 0) {
      states = multicut(lines[i]);
    }
    else if (lines[i].find("\%L") == 0) {
      uniforms = multicut(lines[i]);
    }
    else {
      // definicije
      string name = lines[i].substr(0, lines[i].find(" "));
      string regex = resolve(cut(" ", lines[i]));
      definitions.push_back(definition(name, regex));
    }

    if (rulePos != -1) 
      ++rulePos;
  }

  // zapamti ime pocetnog stanja
  source = states[0];

  // sortiraj stanja za dodjelu brojeva
  sort(states.begin(), states.end());

  // ovdje se imena stanja zamjenjuju brojevima (jer je jednostavnije raditi s njima)
  // getIdx(imeStanja) = broj dodijeljen tom stanju

  // tu krecemo ispisivati kod

  // ispisi pocetno stanje
  printf("const int source = %d;\n", getIdx(source));
  // ispisi broj pravila
  printf("const int nRules = %d;\n", rules.size());
  // ispisi broj stanja
  printf("const int nStates = %d;\n", states.size());

  // novi redak
  puts("");

  // ispisi stanja za pravila, fromState[i] ce biti stanje u kojem se prepoznaje pravilo i
  printf("const int fromState[] = {\n");

  for (int i = 0; i < rules.size(); ++i) 
    printf("  %d%s\n", getIdx(rules[i].state), i + 1 == rules.size() ? "" : ",");

  printf("};\n");
  puts("");
 
  // ispisi stanje u koje vodi pojedino pravilo, toState[i] = stanje u koje vodi pravilo i 
  printf("const int toState[] = {\n");

  for (int i = 0; i < rules.size(); ++i) 
    printf("  %d%s\n", getIdx(rules[i].toState), i + 1 == rules.size() ? "" : ",");

  printf("};\n");
  puts("");
 
  // ispisi ispisi koliko procitanih znakova se racuna za pojedino pravilo... 
  printf("const int goBack[] = {\n");

  for (int i = 0; i < rules.size(); ++i) 
    printf("  %d%s\n", rules[i].goBack, i + 1 == rules.size() ? "" : ",");

  printf("};\n");
  puts("");
 
  // ispisi regularni izraz za svako pravilo 
  printf("const char* regex[] = {\n");

  for (int i = 0; i < rules.size(); ++i) 
    printf("  \"%s\"%s\n", escaped(rules[i].regex).c_str(), i + 1 == rules.size() ? "" : ",");

  printf("};\n");
  puts("");
 
  // ispisi uniformne znakove za svakko pravilo 
  printf("const char* uniforms[] = {\n");

  for (int i = 0; i < rules.size(); ++i) 
    printf("  \"%s\"%s\n", rules[i].name.c_str(), i + 1 == rules.size() ? "" : ",");

  printf("};\n");
  puts("");

  // ispisi flagove za nove redove...
  printf("const bool newLine[] = {\n");

  for (int i = 0; i < rules.size(); ++i)
    printf("  %d%s\n", rules[i].newLine, i + 1 == rules.size() ? "" : ",");
  
  printf("};\n");
  puts("");

  // ispisi ostatak koda koji koristi gore ispisane konstantne nizove
  puts(bp);

  return 0;
}
