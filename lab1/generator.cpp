#include <cstdio>
#include <cstring>
#include <cstdlib>

#include <string>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

//#define LL_DEBUG
#include "log.h"

#define FORS(i, s, z) for (int i = s.find(z); i != string::npos; i = s.find(z, i + 1))

// Ovaj string je kod iz analizator_bp.cpp i ispisuje
// se nakon nizova koji ovise o jeziku koji analiziramo.
// ==================================
const char bp[] = 
  "#include <cstdio>\n" 
  "#include <cstring>\n" 
  "\n" 
  "#include \"automata.h\"\n" 
  "\n" 
  "//#define LL_DEBUG\n" 
  "#include \"log.h\"\n" 
  "\n" 
  "using std::vector;\n" 
  "using std::string;\n" 
  "using std::pair;\n" 
  "using std::make_pair;\n" 
  "\n" 
  "#define MAXBUFF 100000\n" 
  "\n" 
  "// Definicije jezika prema kojima se analizator ravna.\n" 
  "\n" 
  "\n" 
  "/*\n" 
  "a[s] = lista (automat, pravilo) gdje automat prepoznaje\n" 
  "stringove koji odgovaraju regularnom izrazu pravila.\n" 
  "*/\n" 
  "vector< pair< Automata*, int > > a[nStates];\n" 
  "\n" 
  "\n" 
  "/*\n" 
  "Za svako pravilo izgradi inicijaliziraj automat i pohrani ga u listu\n" 
  "za odgovarajuce stanje.\n" 
  "*/\n" 
  "void initAutomata() {\n" 
  "  for (int i = 0; i < nRules; ++i) {\n" 
  "    a[fromState[i]].push_back(make_pair(new Automata(string(regex[i])), i));\n" 
  "  }\n" 
  "}\n" 
  "\n" 
  "// stvari za ucitavanje\n" 
  "\n" 
  "// pomocni buffer za fread\n" 
  "char buffer[MAXBUFF];\n" 
  "\n" 
  "// nakon poziva readInput() sve je ucitano u input\n" 
  "string input = \"\";\n" 
  "\n" 
  "void readInput() {\n" 
  "  while (fread(buffer, 1, MAXBUFF, stdin))\n" 
  "    input += string(buffer);\n" 
  "}\n" 
  "\n" 
  "// kraj stvari za ucitavanje\n" 
  "\n" 
  "// stanje analizatora\n" 
  "int state = source;\n" 
  "\n" 
  "// pozicija u inputu\n" 
  "int pos;\n" 
  "\n" 
  "// brojac linija\n" 
  "int line;\n" 
  "\n" 
  "// resetiraj sve automate za trenutno stanje\n" 
  "void clearAutomata() {\n" 
  "  for (int i = 0; i < a[state].size(); ++i)\n" 
  "    a[state][i].first->clear();\n" 
  "}\n" 
  "\n" 
  "int maxPos;\n" 
  "int matchRule;\n" 
  "int alive;\n" 
  "\n" 
  "/*\n" 
  "Obradi znak c i vrati true ako neki automat dodatkom\n" 
  "tog znaka prihvaca trenutni string.\n" 
  "U tom slucaju globalna varijabla matchRule sadrzi\n" 
  "indeks pravila za koje je automat prihvatio string.\n" 
  "\n" 
  "alive kaze koliko automata jos moze prihvatiti.\n" 
  "Sluzi kao optimizacija, jer u slucaju da nijedan automat ne\n" 
  "moze prihvatiti ne moramo isprobavati daljnje znakove.\n" 
  "*/\n" 
  "bool process(char c) {\n" 
  "  bool ret = false;\n" 
  "  int newAlive = 0;\n" 
  "  // predaj znak c svim automatima za trenutno stanje (state)\n" 
  "  for (int i = a[state].size() - 1; i >= 0; --i) {\n" 
  "    if (a[state][i].first->next(c)) {\n" 
  "      // ovaj automat je prihvatio, pamtimo indeks pravila\n" 
  "      // koje taj automat prepoznaje\n" 
  "      matchRule = a[state][i].second;\n" 
  "      ret = true;\n" 
  "    }\n" 
  "    // provjeri moze li ovaj automat ikada u buducnosti, ako\n" 
  "    // moze onda racunaj da je \"ziv\"\n" 
  "    if (!a[state][i].first->lost())\n" 
  "      ++newAlive;\n" 
  "  }\n" 
  "  alive = newAlive;\n" 
  "  return ret;\n" 
  "}\n" 
  "\n" 
  "// nadji najdulji prefiks od [pos:] koji valja\n" 
  "void maxMatch() {\n" 
  "  // najdalja pozicija do koje je neki automat prihvatio\n" 
  "  maxPos = -1;\n" 
  "  // u pocetku su svi automati \"zivi\"\n" 
  "  alive = a[state].size();\n" 
  "  // idi po inputu dok ima zivih automata i obradi znakove,\n" 
  "  // pamti najdalju poziciju do koje je neki automat prihvatio\n" 
  "  for (int i = pos; i < input.size() && alive; ++i)\n" 
  "    if (process(input[i]))\n" 
  "      maxPos = i;\n" 
  "}\n" 
  "\n" 
  "int main(void)\n" 
  "{\n" 
  "  initAutomata();\n" 
  "  readInput();\n" 
  "\n" 
  "  // dok nije obradjen cijeli string...\n" 
  "  while (pos < input.size()) {\n" 
  "    // pronadji najdulji prefiks koji odgovara nekom pravilu\n" 
  "    maxMatch();\n" 
  "    if (maxPos == -1) {\n" 
  "      // nijedno pravilo se ne moze primijeniti,\n" 
  "      // primijeni oporavak od pogreske preskakanjem trenutnog znaka\n" 
  "      ++pos;\n" 
  "    } else {\n" 
  "      // primijeni pravilo i ispisi ako ima ime\n" 
  "      int npos = 0;\n" 
  "\n" 
  "      // ako ovo pravilo kaze da se moramo vratiti unatrag, onda se vratimo\n" 
  "      if (goBack[matchRule] != -1)\n" 
  "        npos = pos + goBack[matchRule];\n" 
  "      // inace idemo od prvog sljedeceg znaka\n" 
  "      else\n" 
  "        npos = maxPos + 1;\n" 
  "\n" 
  "      // ako ovo pravilo ima ime (odgovarajuci uniformni znak), ispisi ime, liniju i\n" 
  "      // niz znakova koji je pronadjen\n" 
  "      if (uniforms[matchRule][0] != '-')\n" 
  "        printf(\"%s %d %s\\n\", uniforms[matchRule], line + 1, input.substr(pos, npos - pos).c_str());\n" 
  "\n" 
  "      // ako je prepoznat novi redak, povecaj brojac redaka\n" 
  "      if (newLine[matchRule])\n" 
  "        ++line;\n" 
  "\n" 
  "      // ako je potrebno promijeniti stanje zbog pravila koje primjenjujemo\n" 
  "      // promijenimo stanje kako pravilo kaze.\n" 
  "      if (toState[matchRule] != -1)\n" 
  "        state = toState[matchRule];\n" 
  "\n" 
  "      // osvjezi trenutnu poziciju\n" 
  "      pos = npos;\n" 
  "    }\n" 
  "\n" 
  "    // ocisti automate za sljedeci prolaz\n" 
  "    clearAutomata();\n" 
  "  }\n" 
  "\n" 
  "  return 0;\n" 
  "}\n" ;
// ==================================

// jedan redak
string line;
// pocetno stanje
string source;
// lista svih redaka
vector< string > lines;
// lista svih stanja
vector< string > states;
// lista uniformnih znakova
vector< string > uniforms;

// opis pravila

struct rule {
  // stanje u kojem se primjenjuje pravilo
  string state;
  // regularni izraz za prepoznavanje pravila
  string regex;

  // ime (uniformnog znaka) ako postoji, inace "-"
  string name;
  // u koje stanje vodi ovo pravilo ($ ako ne vodi nikamo)
  string toState;

  // znaci li ovo novi redak?
  bool newLine;
  // ako se ne vracamo natrag onda -1, inace broj
  // znakova koji se trebaju smatrati procitanim nakon
  // primjene pravila
  int goBack;

  rule() { clear(); }

  void clear() {
    name = "-";
    toState = "$";
    newLine = false;
    goBack = -1;
  }
};

// lista pravila
vector< rule > rules;

// jedna definicija... ima ime i regularni izraz
struct definition {
  string name;
  string regex;

  definition(string n, string r) {
    name = n;
    regex = r;
  }
};

// lista svih definicija
vector< definition > definitions;

// funkcija za izrezivanje prefiksa od pat
// sve do prve pojave stringa s
string cut(string pat, string s) {
  int pos = s.find(pat) + pat.length();
  return s.substr(pos, s.length() - pos);
}

// funkcija za izrezivanje komada stringova po razmacima
// nista pametno, moze se na 1000 nacina
vector< string > multicut(string s) {
  vector< string > ret;
  FORS (i, s, " ") {
    int to = s.find(" ", i + 1);
    if (to == string::npos) to = s.size();
    ret.push_back(s.substr(i + 1, to - i - 1));
  }
  return ret;
}

// vraca regularni izraz koji odgovara definiciji s
string get(string s) {
  for (int i = 0; i < definitions.size(); ++i) 
    if (definitions[i].name == s) 
      return definitions[i].regex;
  return "$$ ERROR $$";
}

// pretvori regularnu definiciju u regularni izraz
// uzastopnim zamjenama... nista pametno, samo direktna implementacija

string resolve(string s) {
  vector< bool > e;
  e.push_back(0);
  // pronadji koji znakove koji su posebni, e[i] == 1 ako je i-ti znak poseban
  for (int i = 1; i < s.size(); ++i) {
    if (e[i - 1] || s[i - 1] != '\\') 
      e.push_back(0);
    else
      e.push_back(1);
  }
  // dijelovi stringa koje treba zamijeniti
  vector< pair< int, int > > subs;
  FORS (i, s, "{") {
    if (e[i]) continue;
    int next = s.find("}", i);
    // dodaj isjecak koji treba zamijeniti
    subs.push_back(make_pair(i, next - i + 1));
  }
  // idi kroz isjecke unatrag i zamijeni...
  for (int i = subs.size() - 1; i >= 0; --i) 
    s.replace(subs[i].first, subs[i].second, 
        "(" + get(s.substr(subs[i].first, subs[i].second)) + ")");
  return s;
}

// pozicija stringa state u nizu states
int getIdx(string state) {
  int ret = lower_bound(states.begin(), states.end(), state) - states.begin();
  if (ret == states.size() || state == "$")
    return -1;
  return ret;
}

// za posebne znakove kao npr. " koje treba pisati kao \" ili
string escaped(string s) {
  string ret = "";
  for (int i = 0; i < s.size(); ++i) {
    if (s[i] == '\\' || s[i] == '"')
      ret += '\\';
    ret += s[i];
  }
  return ret;
}

int main(void)
{
  // otvori fajl koji generiramo
  freopen("analizator/analizator.cpp", "w", stdout);

  // ucitaj cijeli ulaz liniju po liniju
  while (getline(cin, line)) 
    lines.push_back(line);

  rule r;

  /**
    Ovo je obicno parsiranje, dakle nista pametno.
    Jednostavno ucitavamo opis jezika u formatu opisanom u uputi.
    Svako pravilo koje prepoznamo ubacimo u listu pravila r.
    */

  int rulePos = -1;
  for (int i = 0; i < lines.size(); ++i) {
    if (rulePos == 2) {
      r.name = lines[i];
    }
    else if (lines[i] == "NOVI_REDAK") {
      r.newLine = true;
    }
    else if (lines[i].find("UDJI") == 0) {
      r.toState = cut(" ", lines[i]);
    }
    else if (lines[i].find("VRATI_SE") == 0) {
      r.goBack = atoi(cut(" ", lines[i]).c_str());
    }
    else if (lines[i] == "}") {
      rulePos = -1;
      rules.push_back(r);
      r.clear();
    }
    else if (lines[i] == "{") {
    }
    else if (lines[i][0] == '<') {
      rulePos = 0;
      r.state = lines[i].substr(1, lines[i].find(">") - 1);
      r.regex = resolve(cut(">", lines[i]));
    }
    else if (lines[i].find("\%X") == 0) {
      states = multicut(lines[i]);
    }
    else if (lines[i].find("\%L") == 0) {
      uniforms = multicut(lines[i]);
    }
    else {
      // definicije
      string name = lines[i].substr(0, lines[i].find(" "));
      string regex = resolve(cut(" ", lines[i]));
      definitions.push_back(definition(name, regex));
    }

    if (rulePos != -1) 
      ++rulePos;
  }

  // zapamti ime pocetnog stanja
  source = states[0];

  // sortiraj stanja za dodjelu brojeva
  sort(states.begin(), states.end());

  // ovdje se imena stanja zamjenjuju brojevima (jer je jednostavnije raditi s njima)
  // getIdx(imeStanja) = broj dodijeljen tom stanju

  // tu krecemo ispisivati kod

  // ispisi pocetno stanje
  printf("const int source = %d;\n", getIdx(source));
  // ispisi broj pravila
  printf("const int nRules = %d;\n", rules.size());
  // ispisi broj stanja
  printf("const int nStates = %d;\n", states.size());

  // novi redak
  puts("");

  // ispisi stanja za pravila, fromState[i] ce biti stanje u kojem se prepoznaje pravilo i
  printf("const int fromState[] = {\n");

  for (int i = 0; i < rules.size(); ++i) 
    printf("  %d%s\n", getIdx(rules[i].state), i + 1 == rules.size() ? "" : ",");

  printf("};\n");
  puts("");
 
  // ispisi stanje u koje vodi pojedino pravilo, toState[i] = stanje u koje vodi pravilo i 
  printf("const int toState[] = {\n");

  for (int i = 0; i < rules.size(); ++i) 
    printf("  %d%s\n", getIdx(rules[i].toState), i + 1 == rules.size() ? "" : ",");

  printf("};\n");
  puts("");
 
  // ispisi ispisi koliko procitanih znakova se racuna za pojedino pravilo... 
  printf("const int goBack[] = {\n");

  for (int i = 0; i < rules.size(); ++i) 
    printf("  %d%s\n", rules[i].goBack, i + 1 == rules.size() ? "" : ",");

  printf("};\n");
  puts("");
 
  // ispisi regularni izraz za svako pravilo 
  printf("const char* regex[] = {\n");

  for (int i = 0; i < rules.size(); ++i) 
    printf("  \"%s\"%s\n", escaped(rules[i].regex).c_str(), i + 1 == rules.size() ? "" : ",");

  printf("};\n");
  puts("");
 
  // ispisi uniformne znakove za svakko pravilo 
  printf("const char* uniforms[] = {\n");

  for (int i = 0; i < rules.size(); ++i) 
    printf("  \"%s\"%s\n", rules[i].name.c_str(), i + 1 == rules.size() ? "" : ",");

  printf("};\n");
  puts("");

  // ispisi flagove za nove redove...
  printf("const bool newLine[] = {\n");

  for (int i = 0; i < rules.size(); ++i)
    printf("  %d%s\n", rules[i].newLine, i + 1 == rules.size() ? "" : ",");
  
  printf("};\n");
  puts("");

  // ispisi ostatak koda koji koristi gore ispisane konstantne nizove
  puts(bp);

  return 0;
}
