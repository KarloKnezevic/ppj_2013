#define log(...) fprintf(stderr, ##__VA_ARGS__)

#ifdef LL_ERROR
#define elog(...) fprintf(stderr, ##__VA_ARGS__)
#else
#define elog(...)
#endif

#ifdef LL_DEBUG
#define dlog(...) fprintf(stderr, ##__VA_ARGS__)
#else
#define dlog(...)
#endif

