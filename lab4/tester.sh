#!/bin/bash

for f in $1/* 
do
  cp $f/test.in .
  cp $f/test.out .
  ./generator < test.in > ttt 2>/dev/null
  node main.js ttt 2>/dev/null > outf
  echo "testiram $f:"
  diff -qb outf test.out
  if [ $? -ne '0' ]
  then
    echo "greska!"
    exit 0
  else
    echo "OK!"
  fi

done
