#include <cstdio>
#include <cassert>

#include <set>
#include <map>
#include <stack>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

vector< vector< pair< int, pair< int, int > > > > A;
vector< vector< int > > G;

char uniform[100000];
char snippet[100000];
int line;
int alloc;

vector< int > who;
vector< vector< int > > tree;
stack< int > s, stk;
map< string, int > idx;

struct Uniform {
  string name;
  string text;
  int line;

  Uniform(string n, string t, int l) {
    name = n;
    text = t;
    line = l;
  }

  void output() {
    printf("%s %d %s\n", name.c_str(), line, text.c_str());
  }
};

set< int > syncs;
vector< Uniform > u;
bool error = false;

bool go(int id, int uid) {
  if (error) {
    if (syncs.find(id) == syncs.end()) {
      return 0;
    }
    //printf("imam sync %s\n", character[id]);
    while (A[s.top()][id].first == -1) {
      stk.pop();
      s.pop();
    }
    //printf("na vrhu je %d\n", s.top());
  }
  for (;;) {
    if (A[s.top()][id].first == -1) {
      //printf("greska\n");
      //printf("na vrhu je %d\n", s.top());
      //break;
      if (syncs.find(id) == syncs.end()) {
        return 0;
      }
      //printf("imam sync %s\n", character[id]);
      while (A[s.top()][id].first == -1) {
        stk.pop();
        s.pop();
      }
    }
    if (A[s.top()][id].first == 3) {
      //printf("ACCEPT!\n");
      tree.push_back(vector< int >());
      tree[alloc].push_back(stk.top());
      stk.pop();
      return 1;
    }
    else if (A[s.top()][id].first == 0) {
      int len = A[s.top()][id].second.first;
      int nt = A[s.top()][id].second.second;
      tree.push_back(vector< int >());
      //printf("popam %d\n", len);
      if (len == 0) {
        tree[alloc].push_back(0);
      }
      for (int j = 0; j < len; ++j) {
        tree[alloc].push_back(stk.top());
        stk.pop();
        s.pop();
      }
      //printf("nt = %s\n", character[nt]);
      //printf(".pusham %d\n", G[s.top()][nt]);
      s.push(G[s.top()][nt]);
      stk.push(alloc);
      who.push_back(nt);
      ++alloc;
      //printf("stanje:\n");
      //puts(states[s.top()]);
    }
    else {
      //printf("pusham %d\n", A[s.top()][id].second.first);
      s.push(A[s.top()][id].second.first);
      tree.push_back(vector< int >());
      who.push_back(-uid);
      stk.push(alloc);
      ++alloc;
      //printf("stanje:\n");
      //puts(states[s.top()]);
      break;
    }
  }
  return 1;
}

void output(int n, int depth) {
  for (int i = 0; i < depth; ++i)
    printf(" ");
  if (who[n] < 0)
    u[-1 - who[n]].output();
  else
    printf("%s\n", character[who[n]]);
}

void inorder(int n, int depth) {
  if (n != alloc) output(n, depth);
  for (int i = tree[n].size() - 1; i >= 0; --i)
    inorder(tree[n][i], depth + 1);
}

int main(void)
{
  A.resize(state_cnt + 1);
  G.resize(state_cnt + 1);

  for (int i = 0; i < state_cnt + 1; ++i) {
    A[i].resize(character_cnt + 1);
    G[i].resize(character_cnt + 1);

    for (int j = 0; j <= character_cnt; ++j) {
      A[i][j] = make_pair(-1, make_pair(-1, -1));
      G[i][j] = -1;
    }
  }

  for (int i = 0; i < actions_cnt; ++i) 
    A[actions[i][1]][actions[i][2]] = make_pair(actions[i][0], make_pair(actions[i][3], actions[i][4]));
  
  for (int i = 0; i < gotos_cnt; ++i)
    G[gotos[i][0]][gotos[i][2]] = gotos[i][1];

  for (int i = 0; i < character_cnt; ++i)
    idx[string(character[i])] = i;

  for (int i = 0; i < sync_cnt; ++i)
    syncs.insert(_sync[i]);
  /********

  printf("[%10s]", "");
  for (int j = 0; j < character_cnt; ++j) {
    printf("[%10s]", character[j]);
  }
  putchar('\n');
  for (int i = 0; i <= state_cnt; ++i) {
    printf(":%10d:", i);
    for (int j = 0; j < character_cnt; ++j) {
      printf("%5d,%5d.", A[i][j].first, A[i][j].second.first);
    }
    putchar('\n');
  }
  putchar('\n');
  printf("[%10s]", "");
  for (int j = 0; j < character_cnt; ++j) {
    printf("[%10s]", character[j]);
  }
  putchar('\n');
  for (int i = 0; i <= state_cnt; ++i) {
    printf(":%10d:", i);
    for (int j = 0; j < character_cnt; ++j) {
      printf("%12d", G[i][j]);
    }
    putchar('\n');
  }
  */

  tree.push_back(vector< int >());
  who.push_back(idx["$"]);
  ++alloc;

  s.push(1);
  while (scanf("%s %d %[^\n]\n", uniform, &line, snippet) != EOF) {
    u.push_back(Uniform(string(uniform), string(snippet), line));
    if (!go(idx[string(uniform)], u.size())) {
      error = true;
      continue;
    }
    else 
      error = false;
  }

  go(idx[""], 0);

  //printf("%d\n", s.size());
  //printf("%d\n", s.top());

  inorder(alloc, -1);

  return 0;
}
