#include <cstdio>
#include <cstring>
#include <cassert>
#include <ctime>

#include <iostream>

#include <map>
#include <set>
#include <queue>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;
typedef set< int > :: iterator sit;

#define log(...) //fprintf(stderr, ##__VA_ARGS__)
#define fors(s, it) for(sit it = s.begin(); it != s.end(); ++it)
#define forc(s, it) for(State :: iterator it = s.begin(); it != s.end(); ++it)

// TU TREBA ICI STRING KOJI PREDSTAVLJA KOD ANALIZATORA
// ======================================
const char bp[] = 
  "#include <cstdio>\n" 
  "#include <cassert>\n" 
  "\n" 
  "#include <set>\n" 
  "#include <map>\n" 
  "#include <stack>\n" 
  "#include <vector>\n" 
  "#include <string>\n" 
  "#include <algorithm>\n" 
  "\n" 
  "using namespace std;\n" 
  "\n" 
  "vector< vector< pair< int, pair< int, int > > > > A;\n" 
  "vector< vector< int > > G;\n" 
  "\n" 
  "char uniform[100000];\n" 
  "char snippet[100000];\n" 
  "int line;\n" 
  "int alloc;\n" 
  "\n" 
  "vector< int > who;\n" 
  "vector< vector< int > > tree;\n" 
  "stack< int > s, stk;\n" 
  "map< string, int > idx;\n" 
  "\n" 
  "struct Uniform {\n" 
  "  string name;\n" 
  "  string text;\n" 
  "  int line;\n" 
  "\n" 
  "  Uniform(string n, string t, int l) {\n" 
  "    name = n;\n" 
  "    text = t;\n" 
  "    line = l;\n" 
  "  }\n" 
  "\n" 
  "  void output() {\n" 
  "    printf(\"%s %d %s\\n\", name.c_str(), line, text.c_str());\n" 
  "  }\n" 
  "};\n" 
  "\n" 
  "set< int > syncs;\n" 
  "vector< Uniform > u;\n" 
  "bool error = false;\n" 
  "\n" 
  "bool go(int id, int uid) {\n" 
  "  if (error) {\n" 
  "    if (syncs.find(id) == syncs.end()) {\n" 
  "      return 0;\n" 
  "    }\n" 
  "    //printf(\"imam sync %s\\n\", character[id]);\n" 
  "    while (A[s.top()][id].first == -1) {\n" 
  "      stk.pop();\n" 
  "      s.pop();\n" 
  "    }\n" 
  "    //printf(\"na vrhu je %d\\n\", s.top());\n" 
  "  }\n" 
  "  for (;;) {\n" 
  "    if (A[s.top()][id].first == -1) {\n" 
  "      //printf(\"greska\\n\");\n" 
  "      //printf(\"na vrhu je %d\\n\", s.top());\n" 
  "      //break;\n" 
  "      if (syncs.find(id) == syncs.end()) {\n" 
  "        return 0;\n" 
  "      }\n" 
  "      //printf(\"imam sync %s\\n\", character[id]);\n" 
  "      while (A[s.top()][id].first == -1) {\n" 
  "        stk.pop();\n" 
  "        s.pop();\n" 
  "      }\n" 
  "    }\n" 
  "    if (A[s.top()][id].first == 3) {\n" 
  "      //printf(\"ACCEPT!\\n\");\n" 
  "      tree.push_back(vector< int >());\n" 
  "      tree[alloc].push_back(stk.top());\n" 
  "      stk.pop();\n" 
  "      return 1;\n" 
  "    }\n" 
  "    else if (A[s.top()][id].first == 0) {\n" 
  "      int len = A[s.top()][id].second.first;\n" 
  "      int nt = A[s.top()][id].second.second;\n" 
  "      tree.push_back(vector< int >());\n" 
  "      //printf(\"popam %d\\n\", len);\n" 
  "      if (len == 0) {\n" 
  "        tree[alloc].push_back(0);\n" 
  "      }\n" 
  "      for (int j = 0; j < len; ++j) {\n" 
  "        tree[alloc].push_back(stk.top());\n" 
  "        stk.pop();\n" 
  "        s.pop();\n" 
  "      }\n" 
  "      //printf(\"nt = %s\\n\", character[nt]);\n" 
  "      //printf(\".pusham %d\\n\", G[s.top()][nt]);\n" 
  "      s.push(G[s.top()][nt]);\n" 
  "      stk.push(alloc);\n" 
  "      who.push_back(nt);\n" 
  "      ++alloc;\n" 
  "      //printf(\"stanje:\\n\");\n" 
  "      //puts(states[s.top()]);\n" 
  "    }\n" 
  "    else {\n" 
  "      //printf(\"pusham %d\\n\", A[s.top()][id].second.first);\n" 
  "      s.push(A[s.top()][id].second.first);\n" 
  "      tree.push_back(vector< int >());\n" 
  "      who.push_back(-uid);\n" 
  "      stk.push(alloc);\n" 
  "      ++alloc;\n" 
  "      //printf(\"stanje:\\n\");\n" 
  "      //puts(states[s.top()]);\n" 
  "      break;\n" 
  "    }\n" 
  "  }\n" 
  "  return 1;\n" 
  "}\n" 
  "\n" 
  "void output(int n, int depth) {\n" 
  "  for (int i = 0; i < depth; ++i)\n" 
  "    printf(\" \");\n" 
  "  if (who[n] < 0)\n" 
  "    u[-1 - who[n]].output();\n" 
  "  else\n" 
  "    printf(\"%s\\n\", character[who[n]]);\n" 
  "}\n" 
  "\n" 
  "void inorder(int n, int depth) {\n" 
  "  if (n != alloc) output(n, depth);\n" 
  "  for (int i = tree[n].size() - 1; i >= 0; --i)\n" 
  "    inorder(tree[n][i], depth + 1);\n" 
  "}\n" 
  "\n" 
  "int main(void)\n" 
  "{\n" 
  "  A.resize(state_cnt + 1);\n" 
  "  G.resize(state_cnt + 1);\n" 
  "\n" 
  "  for (int i = 0; i < state_cnt + 1; ++i) {\n" 
  "    A[i].resize(character_cnt + 1);\n" 
  "    G[i].resize(character_cnt + 1);\n" 
  "\n" 
  "    for (int j = 0; j <= character_cnt; ++j) {\n" 
  "      A[i][j] = make_pair(-1, make_pair(-1, -1));\n" 
  "      G[i][j] = -1;\n" 
  "    }\n" 
  "  }\n" 
  "\n" 
  "  for (int i = 0; i < actions_cnt; ++i)\n" 
  "    A[actions[i][1]][actions[i][2]] = make_pair(actions[i][0], make_pair(actions[i][3], actions[i][4]));\n" 
  "\n" 
  "  for (int i = 0; i < gotos_cnt; ++i)\n" 
  "    G[gotos[i][0]][gotos[i][2]] = gotos[i][1];\n" 
  "\n" 
  "  for (int i = 0; i < character_cnt; ++i)\n" 
  "    idx[string(character[i])] = i;\n" 
  "\n" 
  "  for (int i = 0; i < sync_cnt; ++i)\n" 
  "    syncs.insert(_sync[i]);\n" 
  "  /********\n" 
  "\n" 
  "  printf(\"[%10s]\", \"\");\n" 
  "  for (int j = 0; j < character_cnt; ++j) {\n" 
  "    printf(\"[%10s]\", character[j]);\n" 
  "  }\n" 
  "  putchar('\\n');\n" 
  "  for (int i = 0; i <= state_cnt; ++i) {\n" 
  "    printf(\":%10d:\", i);\n" 
  "    for (int j = 0; j < character_cnt; ++j) {\n" 
  "      printf(\"%5d,%5d.\", A[i][j].first, A[i][j].second.first);\n" 
  "    }\n" 
  "    putchar('\\n');\n" 
  "  }\n" 
  "  putchar('\\n');\n" 
  "  printf(\"[%10s]\", \"\");\n" 
  "  for (int j = 0; j < character_cnt; ++j) {\n" 
  "    printf(\"[%10s]\", character[j]);\n" 
  "  }\n" 
  "  putchar('\\n');\n" 
  "  for (int i = 0; i <= state_cnt; ++i) {\n" 
  "    printf(\":%10d:\", i);\n" 
  "    for (int j = 0; j < character_cnt; ++j) {\n" 
  "      printf(\"%12d\", G[i][j]);\n" 
  "    }\n" 
  "    putchar('\\n');\n" 
  "  }\n" 
  "  */\n" 
  "\n" 
  "  tree.push_back(vector< int >());\n" 
  "  who.push_back(idx[\"$\"]);\n" 
  "  ++alloc;\n" 
  "\n" 
  "  s.push(1);\n" 
  "  while (scanf(\"%s %d %[^\\n]\\n\", uniform, &line, snippet) != EOF) {\n" 
  "    u.push_back(Uniform(string(uniform), string(snippet), line));\n" 
  "    if (!go(idx[string(uniform)], u.size())) {\n" 
  "      error = true;\n" 
  "      continue;\n" 
  "    }\n" 
  "    else\n" 
  "      error = false;\n" 
  "  }\n" 
  "\n" 
  "  go(idx[\"\"], 0);\n" 
  "\n" 
  "  //printf(\"%d\\n\", s.size());\n" 
  "  //printf(\"%d\\n\", s.top());\n" 
  "\n" 
  "  inorder(alloc, -1);\n" 
  "\n" 
  "  return 0;\n" 
  "}\n" ;
// ======================================

// Input stvari...

// Ucitaj jedan redak
string read_line() {
  string ret;
  getline(cin, ret);
  return cin.eof() ? "" : ret;
}

// Splittaj string s na razmacima, preskoci prvih skip
vector< string > split(string s, int skip) {
  vector< string > ret;
  string now = "";
  for (int i = skip; i < s.size(); ++i) {
    if (s[i] == ' ') {
      if (now != "") {
        if (skip <= 0)
          ret.push_back(now);
        --skip;
      }
      now = "";
    }
    else
      now += s[i];
  }
  if (now != "")
    ret.push_back(now);
  return ret;
}

int V, T;

string line;
string source;
vector< string > v, t, syn;

// Vraca jedinstveni broj koji odgovara znaku s
int get_index(string s) {
  //log("indeks od %s\n", s.c_str());
  int p1 = lower_bound(v.begin(), v.end(), s) - v.begin();
  if (p1 < v.size() && v[p1] == s) return p1 + 1;
  int p2 = lower_bound(t.begin(), t.end(), s) - t.begin();
  if (p2 < t.size() && t[p2] == s) return p2 + V;
  assert(false);
  return -1;
}

// Mapira get_index na vektor stringova
vector< int > get_indicies(vector< string > v) {
  vector< int > ret;
  for (int i = 0; i < v.size(); ++i)
    ret.push_back(get_index(v[i]));
  return ret;
}

// Je li znak c terminal?
bool terminal(int c) { 
  return c > V; 
}

int reduce(int c) {
  return c - V;
}

// Obrnuto od get_index
string get_string(int i) {
  if (i == 0) return "<POMOCNI_POCETNI_ZNAK>";
  if (i < V) return v[i - 1];
  return t[i - V];
}

// broj nonterminala i terminala
int epsilon;

// Produkcije i -> prod[i][j]
vector< vector< vector< int > > > prod;

// Red produkcije u inputu...
vector< vector< int > > prio;

// Stvari za racunanje first(v, t)

// first[i] = skup znakova kojima moze poceti string izveden iz i
vector< set< int > > first;

// Moze li c rezultirati praznim stringom?
bool has_epsilon(int c) {
  return first[c].find(epsilon) != first[c].end();
}

// Napuni s novim znakovima koji se mogu zakluciti iz first i v
// Vraca true ako se v moze razviti u prazan string
bool relax(vector< int > &v, int pos, set< int > &s) {
  int i;
  for (i = pos; i < v.size(); ++i) {
    if (terminal(v[i])) {
      s.insert(v[i]);
      break;
    }
    fors(first[v[i]], it)
      if (*it != epsilon)
        s.insert(*it);
    if (!has_epsilon(v[i]))
      break;
  }
  return i == v.size();
}

// Racuna first skupove za sve nezavrsne znakove
void precalc_first() {
  first.resize(V);
  for (int i = 0; i < V; ++i) 
    for (int j = 0; j < prod[i].size(); ++j) 
      if (terminal(prod[i][j][0])) 
        first[i].insert(prod[i][j][0]);

  bool changed = true;
  while (changed) {
    changed = false;
    for (int i = 0; i < V; ++i) {
      int sz = first[i].size();
      for (int j = 0; j < prod[i].size(); ++j)
        if (relax(prod[i][j], 0, first[i]))
          first[i].insert(epsilon);
      changed |= (sz != first[i].size());
    }
  }
}

// Nadji skup znakova s koji mogu biti prvi znakovi stringa v[pos:]t
void calc_first(vector< int > &w, int pos, int t, set< int > &s) {
  if (relax(w, pos, s)) 
    s.insert(t + V);
}

// Konfiguracije i racunanje closure-a

// Opis jedne konfiguracije:
// left je lijevi dio produkcije 
// pid je indeks u vektoru prod
// la je indeks lookahead znaka
// pos je pozicija na kojoj smo trenutno u produkciji
struct Configuration {
  int left;
  int pid;
  int la;
  int pos;

  Configuration() {}
  Configuration(int _left, int _pid, int _la, int _pos) {
    left = _left;
    pid = _pid;
    la = _la;
    pos = _pos;
    assert(pos <= prod[left][pid].size());
    if (prod[left][pid][0] == epsilon)
      assert(pos == 1);
  }

  int next() const {
    if (pos == prod[left][pid].size()) return -1;
    return prod[left][pid][pos];
  }

  int len() const {
    if (prod[left][pid][0] == epsilon) return 0;
    return prod[left][pid].size();
  }

  int order() const {
    return prio[left][pid];
  }

  void output() const {
    log("%s ", get_string(left).c_str());
    for (int i = 0; i < prod[left][pid].size(); ++i) {
      if (i == pos)
        log(".");
      log("%s ", get_string(prod[left][pid][i]).c_str());
    }
    log(": %s\n\n", get_string(la).c_str());
  }

  void output_str() const {
    printf("\"%s ::> ", get_string(left).c_str());
    for (int i = 0; i < prod[left][pid].size(); ++i) {
      if (i == pos)
        printf(".");
      printf("%s ", get_string(prod[left][pid][i]).c_str());
    }
    printf(": %s\\n\"\n", get_string(la).c_str());
  }

  friend bool operator<(Configuration a, Configuration b) {
    if (a.left != b.left) return a.left < b.left;
    if (a.pid != b.pid) return a.pid < b.pid;
    if (a.la != b.la) return a.la < b.la;
    return a.pos < b.pos;
  }
};

Configuration next(Configuration c) {
  return Configuration(c.left, c.pid, c.la, c.pos + 1);
}

// Stanje je skup konfiguracija
typedef set< Configuration > State;

void output(State &s) {
  forc(s, it) {
    it->output();
  }
}

void output_str(State &s) {
  if (s.empty()) puts("\"\"");
  forc(s, it) {
    it->output_str();
  }
}

// closure[left][right][t] je closure za osnovnu konfiguraciju te produkcije
// s terminalom t
vector< vector< vector< State > > > closure;

void unite(State &a, State &b) {
  forc(b, it) 
    a.insert(*it);
}

void calc_closure(Configuration c, State &s) {
  s.insert(c);
 
  if (c.next() == -1 || terminal(c.next())) 
    return;

  int now = c.next();
  set< int > fs;
  calc_first(prod[c.left][c.pid], c.pos + 1, reduce(c.la), fs);

  fors(fs, it) {
    if (*it == epsilon) continue;
    for (int i = 0; i < prod[now].size(); ++i) {
      unite(s, closure[now][i][reduce(*it)]);
    }
  }
}

void precalc_closure() {
  closure.resize(V);
  for (int i = 0; i < V; ++i) {
    closure[i].resize(prod[i].size());
    for (int j = 0; j < closure[i].size(); ++j) {
      closure[i][j].resize(T);
      for (int k = 0; k < T; ++k) 
        if (k + V != epsilon) 
          closure[i][j][k].insert(Configuration(i, j, k + V, prod[i][j][0] == epsilon));
    }
  }

  bool changed = true;
  while (changed) {
    changed = false;
    for (int i = 0; i < V; ++i) {
      for (int j = 0; j < prod[i].size(); ++j) {
        int now = prod[i][j][0];
        if (terminal(now)) continue;
        for (int k = 0; k < T; ++k) {
          if (k + V == epsilon) continue;

          int sz = closure[i][j][k].size();
          set< int > fs;
          calc_first(prod[i][j], 1, k, fs);

          fors(fs, it) {
            if (*it == epsilon) continue;
            for (int l = 0; l < prod[now].size(); ++l) {
              unite(closure[i][j][k], closure[now][l][reduce(*it)]);
            }
          }

          if (sz != closure[i][j][k].size()) {
            changed = true;
          }
        }
      }
    }
  }
}

struct Action {
  int type;
  int from;
  int on;
  int len;
  int nt;

  Action() {}
  Action(int _type, int _from, int _on, int _len, int _nt) {
    type = _type;
    from = _from;
    on = _on;
    len = _len;
    nt = _nt;
  }
};

struct Goto {
  int from;
  int to;
  int on;

  Goto() {}
  Goto(int _from, int _to, int _on) {
    from = _from;
    to = _to;
    on = _on;
  }
};

vector< Action > actions;
vector< Goto > gotos;

map< State, int > visited;
queue< State > q;

bool cmpfO(Configuration a, Configuration b) {
  if (a.order() != b.order()) return a.order() < b.order();
  return a < b;
}

vector< State > states;

void precalc_dfa() {
  State src;
  calc_closure(Configuration(0, 0, get_index(""), 0), src);
  q.push(src);

  int ticker = 0;
  visited[src] = ++ticker;
  states.push_back(State());
  states.push_back(src);

  while (!q.empty()) {
    State curr = q.front(); q.pop();
    int id = visited[curr];
    vector< State > neighbors(V + T);

    forc(curr, it) {
      if (it->next() != -1) 
        neighbors[it->next()].insert(*it);
      else 
        neighbors[it->la].insert(*it);
    }

    // Inace ne reduciramo i gradimo susjede
    for (int i = 0; i < V + T; ++i) {
      if (neighbors[i].empty()) continue;

      vector< Configuration > reducers;
      forc(neighbors[i], it)
        if (it->next() == -1)
          reducers.push_back(*it);

      if (reducers.size() == neighbors[i].size()) {
        Configuration best = reducers[0];
        for (int j = 0; j < reducers.size(); ++j)
          if (best.order() > reducers[j].order())
            best = reducers[j];
        if (best.left == 0 && best.pos == 1)
          actions.push_back(Action(3, id, best.la, 1, 0));
        else
          actions.push_back(Action(0, id, best.la, best.len(), best.left));
        continue;
      }

      State neighbor;
      forc(neighbors[i], it) {
        if (it->next() != -1)
          calc_closure(next(*it), neighbor);
      }

      if (!visited.count(neighbor)) {
        visited[neighbor] = ++ticker;
        states.push_back(neighbor);
        q.push(neighbor);
      }

      if (terminal(i))
        actions.push_back(Action(1, id, i, visited[neighbor], 0));
      else
        gotos.push_back(Goto(id, visited[neighbor], i));
    }
  }

  printf("const int state_cnt = %d;\n", ticker + 1);
}

void print_states() {
  printf("const char *states[] = {\n");
  for (int i = 0; i < states.size(); ++i) {
    output_str(states[i]);
    printf("%s\n", i == states.size() - 1 ? "" : ",");
  }
  printf("};\n");
}

void print_tables() {
  printf("const int sync_cnt = %d;\n", syn.size());
  printf("const int _sync[sync_cnt] = {\n");
  for (int i = 0; i < syn.size(); ++i)
    printf("  %d%s\n", get_index(syn[i]), i == syn.size() - 1 ? "" : ",");
  printf("};\n");
  printf("const int character_cnt = %d;\n", V + T);
  printf("const char *character[character_cnt] = {\n");
  printf("  \"\", // dummy non-terminal\n");
  for (int i = 0; i < v.size(); ++i)
    printf("  \"%s\",\n", v[i].c_str());
  for (int i = 0; i < t.size(); ++i)
    printf("  \"%s\"%s\n", t[i].c_str(), i == t.size() - 1 ? "" : ",");
  printf("};\n");
  printf("const int actions_cnt = %d;\n", actions.size());
  printf("const int actions[actions_cnt][5] = {\n");
  for (int i = 0; i < actions.size(); ++i)
    printf("  {%d, %d, %d, %d, %d}%s\n", actions[i].type, actions[i].from, 
        actions[i].on, actions[i].len, actions[i].nt, i == actions.size() - 1 ? "" : ",");
  printf("};\n");
  
  printf("const int gotos_cnt = %d;\n", gotos.size());
  printf("const int gotos[gotos_cnt][3] = {\n");
  for (int i = 0; i < gotos.size(); ++i)
    printf("  {%d, %d, %d}%s\n", gotos[i].from, gotos[i].to, 
        gotos[i].on, i == gotos.size() - 1 ? "" : ",");
  printf("};\n");
}

int main(void)
{
  freopen("analizator/analizator.cpp", "w", stdout);
  v = split(read_line(), 1);
  t = split(read_line(), 1);
  syn = split(read_line(), 1);

  // Dodaj epsilon znak!
  t.push_back("$");

  // Ovo cemo gledati kao znak kraja stringa
  t.push_back("");

  source = v[0];
  sort(v.begin(), v.end());
  sort(t.begin(), t.end());
  sort(syn.begin(), syn.end());

  V = v.size() + 1;
  T = t.size();

  log("f je %d\n", get_index(""));
  log(".. %d %d\n", V, T);

  prod.resize(V);
  prio.resize(V);

  epsilon = get_index("$");
  log("epsilon = %d\n", epsilon);

  // Dodaj pomocnu produkciju
  prod[0].push_back(vector< int >(1, get_index(source)));
  prio[0].push_back(-1);

  int left, cprio = 0;
  while ((line = read_line()) != "") {
    if (line[0] != ' ') 
      left = get_index(line);
    else {
      prod[left].push_back(get_indicies(split(line, 0)));
      prio[left].push_back(cprio++);
    }
  }

  int ct;
  ct = clock();
  precalc_first();
  log("first -> %.3lf\n", double(clock() - ct) / CLOCKS_PER_SEC);
  ct = clock();
  precalc_closure();
  log("closure -> %.3lf\n", double(clock() - ct) / CLOCKS_PER_SEC);
  ct = clock();
  precalc_dfa();
  log("dfa -> %.3lf\n", double(clock() - ct) / CLOCKS_PER_SEC);

  print_tables();
  print_states();
  puts(bp);

  return 0;
}
