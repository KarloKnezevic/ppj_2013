#!/bin/bash

function doit() {
  ./generator < $t/test.san
  g++ analizator/*.cpp -o analizator/analizator
  time ./analizator/analizator < $t/test.in > outf
}

for t in test/*
do
  echo "test $t:"
  time doit
  diff -qb $t/test.out outf
done
